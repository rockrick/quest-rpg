
local Vec = require 'common.vec'
local MessageBox = require 'view.message_box'
local NumberIndicator = require 'view.number_indicator'
local SpriteAtlas = require 'view.sprite_atlas'
local SpriteAnimations = require 'view.sprite_animations'
local BattleField = require 'view.battlefield'
local State = require 'state'

local EncounterState = require 'common.class' (State)

local CHARACTER_GAP = 96

local SFX = {}

local ENCOUNTER = {}

local MESSAGES = {
  Fight = "%s attacked %s",
  Skill = "%s unleashed a skill",
  Item = "%s used %s potion.",
  Run = "Ran away successfully",
  Miss = "%s missed attack on %s"
}

local DEATH_MESSAGES = {
  "%s died a horrible death",
  "%s was slain",
  "%s's head exploded",
  "%s has met their maker",
  "%s died a not so horrible death...",
  "%s was murdered",
  "%s will never see the sunlight again",
  "%s stopped moving... forever",
}

function EncounterState:_init(stack)
  self:super(stack)
  self.party = nil
  self.turns = nil
  self.next_turn = nil
  -- inicia os tracks de efeitos sonoros
  SFX.click = love.audio.newSource('assets/sfx/click4.ogg', 'static')
  SFX.attack = {
    love.audio.newSource('assets/sfx/attack1.ogg', 'static'),
    love.audio.newSource('assets/sfx/attack2.ogg', 'static'),
  }
  SFX.crit = love.audio.newSource('assets/sfx/attack3.ogg', 'static')
  SFX.miss = love.audio.newSource('assets/sfx/miss.wav', 'static')
  SFX.skill = love.audio.newSource('assets/sfx/skill.wav', 'static')
  SFX.item = love.audio.newSource('assets/sfx/item.wav', 'static')
end

-- ao entrar num encontro, carrega os personagens vivos da party,
-- e os inimigos do encontro
function EncounterState:enter(params)
  local atlas = SpriteAtlas()
  local animations = SpriteAnimations(atlas)
  local battlefield = BattleField()
  local bfbox = battlefield.bounds
  local message = MessageBox(Vec(bfbox.left, bfbox.bottom + 16))
  local death_message = MessageBox(Vec(bfbox.left, bfbox.bottom + 48))
  local indicator = NumberIndicator()
  local n = 0
  local party_origin = battlefield:east_team_origin()
  self.turns = {}
  self.next_turn = 1
  self.party = params.party
  -- processa os personagens da party
  for i, character in ipairs(params.party) do
    local pos = party_origin + Vec(0, 1) * CHARACTER_GAP * (i - 1)
    -- determina o tipo do personagem e sua iniciativa
    character:set_type('ally')
    character:generate_initiative()
    self.turns[i] = character
    -- nao insere aliados mortos no atlas
    if character:is_alive() then
      atlas:add(character, pos, character:get_appearance())
    end
    n = n + 1
  end
  local encounter_origin = battlefield:west_team_origin()
  -- processa os personagens do encontro
  ENCOUNTER = params.encounter
  for i, character in ipairs(params.encounter) do
    local pos = encounter_origin + Vec(0, 1) * CHARACTER_GAP * (i - 1)
    character:set_type('enemy')
    character:generate_initiative()
    self.turns[n + i] = character
    atlas:add(character, pos, character:get_appearance())
  end
  -- ordena a lista de turnos de acordo com a iniciativa
  table.sort(self.turns, self.compare)
  self:view():add('atlas', atlas)
  self:view():add('animations', animations)
  self:view():add('battlefield', battlefield)
  self:view():add('message', message)
  self:view():add('death_message', death_message)
  self:view():add('number_indicator', indicator)
  message:set("You stumble upon an encounter")
end

-- função de comparação para ordenar os personagens por iniciativa
function EncounterState.compare(char_a, char_b)
  return char_a:get_initiative() > char_b:get_initiative()
end

function EncounterState:leave()
  self:view():get('atlas'):clear()
  self:view():remove('atlas')
  self:view():remove('animations')
  self:view():remove('battlefield')
  self:view():remove('message')
  self:view():remove('death_message')
  self:view():remove('number_indicator')
end

function EncounterState:update(_)
  local current_character = self.turns[self.next_turn]
  self.next_turn = self.next_turn % #self.turns + 1
  -- pula o turno de personagens mortos, pois eles nao jogam
  while not current_character:is_alive() do
    current_character = self.turns[self.next_turn]
    self.next_turn = self.next_turn % #self.turns + 1
  end
  local params = { current_character = current_character }
  local next_state
  if current_character:get_type() == 'ally' then
    params.encounter = ENCOUNTER
    params.party = self.party
    next_state = 'player_turn'
  elseif current_character:get_type() == 'enemy' then
    params.encounter = self.party
    params.party = ENCOUNTER
    next_state = 'enemy_turn'
  end
  return self:push(next_state, params)
end

-- processa os parametros devolvidos pelo ultimo estado, checando qual foi
-- a ação efetuada
function EncounterState:resume(params)
  self:view():get('death_message'):set(nil)
  local message, death_message
  local option = params.action
  if not params.resume then
    if option == 'Fight' then
      -- toca os sons
      if not params.damage then
        -- errou o golpe, muda a mensagem
        option = 'Miss'
        SFX.miss:stop()
        SFX.miss:play()
      else
        if params.crit then
          SFX.crit:stop()
          SFX.crit:play()
        else
          -- escolhe um som aleatorio para o dano,
          -- dentre os sfx's de dano
          local r = math.random(#SFX.attack)
          SFX.attack[r]:stop()
          SFX.attack[r]:play()
        end
      end
      -- usa o number indicator para mostrar o dano,
      -- efeito da skill ou item
      self:display_number(params, 'damage')
      message = MESSAGES[option]:format(params.character:get_name(),
      params.target:get_name())
      -- mostra mensagem de morte
      if not params.target:is_alive() then
        death_message = DEATH_MESSAGES[math.random(#DEATH_MESSAGES)]
        death_message = death_message:format(params.target:get_name())
        self:view():get('death_message'):set(death_message)
      end
    elseif option == 'Skill' then
      SFX.skill:stop()
      SFX.skill:play()
      if params.skill.atk then
        self:display_number(params, 'damage')
      end
      if params.skill.heal or params.skill.eff == 'drain' then
        self:display_number(params, 'heal')
      end
      message = params.message:format(params.character:get_name(),
                                      params.target:get_name())
    elseif option == 'Item' then
      SFX.item:stop()
      SFX.item:play()
      if params.item.buff == 'heal' then
        params.heal = params.item.quantity
        self:display_number(params, 'heal')
      elseif params.item.buff == 'mana' then
        params.mana = params.item.quantity
        self:display_number(params, 'mana')
      elseif params.item.buff == 'berserker' then
        self:display_number(params, 'berserker')
      end
      message = params.message:format(params.character:get_name())
    else
      message = MESSAGES[option]
    end
    self:view():get('message'):set(message)
  end

  -- se todos inimigos morrerem ou fugirmos, termina o encontro atual
  if self.check_end_condition(params.enemies) or params.action == 'Run' then
    -- se nao tiver acabado de voltar de uma pausa, entra na pausa
    if not params.resume then
      return self:push('state_pause', params)
    end
    -- caso contrario, sai do encontro e vai pro proximo
    local ret_params = { game_over = self:check_game_over() }
    return self:pop(ret_params)
  end
end

-- mostra os numeros na tela, de dano, mana, heal, ou outros efeitos
function EncounterState:display_number(params, type)
  local atlas = self:view():get('atlas')
  local indicator = self:view():get('number_indicator')
  local target_sprite
  if type == 'damage' then
    target_sprite = atlas:get(params.target)
    indicator:show_damage(target_sprite, params.damage, params.crit)
  elseif type == 'heal' then
    target_sprite = atlas:get(params.healed_char or params.character)
    indicator:show_heal(target_sprite, params.heal)
  elseif type == 'mana' then
    target_sprite = atlas:get(params.character)
    indicator:show_mana(target_sprite, params.mana)
  elseif type == 'berserker' then
    target_sprite = atlas:get(params.character)
    indicator:show_berserker(target_sprite)
  else
    error('Invalid type for number display')
  end
end

-- checa se o jogo acabou (todos os aliados morreram)
function EncounterState:check_game_over()
  for _, ally in ipairs(self.party) do
    if ally:is_alive() then
      return false
    end
  end
  return true
end

-- checa se todos inimigos morreram
function EncounterState.check_end_condition(enemies)
  if enemies == nil then return false end
  for _, enemy in ipairs(enemies) do
    if enemy:is_alive() then
      return false
    end
  end
  return true
end

return EncounterState

