
local Vec = require 'common.vec'
local CharacterStats = require 'view.character_stats'
local TurnCursor = require 'view.turn_cursor'
local State = require 'state'


local EnemyTurnState = require 'common.class' (State)

function EnemyTurnState:_init(stack)
  self:super(stack)
  self.character = nil
  self.enemies = {}
  self.cursor = nil
  self.atlas = nil
  self.animations = nil
end

function EnemyTurnState:enter(params)
  self.character = params.current_character
  self.enemies = params.encounter
  self.atlas = self:view():get('atlas')
  self.animations = self:view():get('animations')
  self:_show_cursor()
  self:_show_stats()
  -- inimigo vai direto pro state pause,
  -- e executa sua ação logo depois
  self:push('state_pause', {time = 1.2})
end

function EnemyTurnState:_show_cursor()
  local sprite_instance = self.atlas:get(self.character)
  self.cursor = TurnCursor(sprite_instance)
  self:view():add('turn_cursor', self.cursor)
end

function EnemyTurnState:_show_stats()
  local bfbox = self:view():get('battlefield').bounds
  local position = Vec(bfbox.right + 16, bfbox.top)
  local char_stats = CharacterStats(position, self.character)
  self:view():add('char_stats', char_stats)
end

function EnemyTurnState:leave()
  self:view():remove('turn_menu')
  self:view():remove('turn_cursor')
  self:view():remove('char_stats')
end

-- inimigo toma ação
-- por ora, os inimigos escolhem um alvo aleatorio para atacarem
function EnemyTurnState:enemy_action(targets)
  local r = math.random(1, #targets)
  local target = targets[r]
  while not target:is_alive() do
    r = math.random(1, #targets)
    target = targets[r]
  end
  local result = self.character:attack(target)
  if result.damage then
    self.animations:attack(self.character)
    self.animations:take_damage(target)
  end
  return self:pop({
    action = 'Fight',
    character = self.character,
    target = target,
    enemies = self.enemies,
    damage = result.damage,
    crit = result.crit,
  })
end

-- ao voltar do state pause, o inimigo executa sua ação
function EnemyTurnState:resume(params)
  if params.resume then
    self:enemy_action(self.enemies)
  end
end

return EnemyTurnState
