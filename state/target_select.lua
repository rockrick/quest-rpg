
local TurnCursor = require 'view.turn_cursor'
local State = require 'state'

local SFX = {}

local TargetSelectState = require 'common.class' (State)

-- Estado de seleção de alvos
-- Recebe uma lista de possiveis alvos,
-- mostra um cursor para a escolha do alvo
-- e devolve o alvo escolhido pelo jogador
function TargetSelectState:_init(stack)
  self:super(stack)
  self.character = nil
  self.targets = {}
  self.current_target = 1
  self.target_cursor = nil
  self.color = nil
  self.atlas = nil
  SFX.click = love.audio.newSource('assets/sfx/click4.ogg', 'static')
end

function TargetSelectState:enter(params)
  if params.color then
    self.color = params.color
  else
    self.color = 'red'
  end
  self.targets = params.targets
  for i, instance in ipairs(self.targets) do
    if instance:is_alive() then
      self.current_target = i
      break
    end
  end
  self.atlas = self:view():get('atlas')
  self:_show_target_cursor()
end

function TargetSelectState:next()
  local new_target = self.current_target + 1
  while new_target <= #self.targets and not self.targets[new_target]:is_alive() do
    new_target = new_target + 1
  end
  if new_target > #self.targets then return end
  self.current_target = new_target
  self:_show_target_cursor()
end

function TargetSelectState:previous()
  local new_target = self.current_target - 1
  while new_target >= 1 and not self.targets[new_target]:is_alive() do
    new_target = new_target - 1
  end
  if new_target < 1 then return end
  self.current_target = new_target
  self:_show_target_cursor()
end

function TargetSelectState:_show_target_cursor()
  self:view():remove('target_cursor')
  local target = self.targets[self.current_target]
  local enemy_atlas_instance = self.atlas:get(target)
  self.target_cursor = TurnCursor(enemy_atlas_instance, self.color)
  self:view():add('target_cursor', self.target_cursor)
end

function TargetSelectState:on_keypressed(key)
  if key == 'up' then
    SFX.click:stop()
    SFX.click:play()
    self:previous()
  elseif key == 'down' then
    SFX.click:stop()
    SFX.click:play()
    self:next()
  elseif key == 'return' then
    return self:pop({target = self.targets[self.current_target], cancel = false})
  elseif key == 'escape' then
    return self:pop({target = self.targets[self.current_target], cancel = true})
  end
end

function TargetSelectState:leave()
  self.atlas = nil
  self:view():remove('target_cursor')
end

return TargetSelectState