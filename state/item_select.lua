
local ListMenu = require 'view.list_menu'
local State = require 'state'

local ITEMS = {} -- item tables

local SFX = {}

local ItemSelectState = require 'common.class' (State)

function ItemSelectState:_init(stack)
  self:super(stack)
  self.menu = nil
  self.items = {}
  self.item = nil
  self.character = nil
  SFX.click = love.audio.newSource('assets/sfx/click4.ogg', 'static')
end

function ItemSelectState:enter(params)
  local item_menu = {}
  self.character = params.character
  for i, item in pairs(params.item_names) do
    ITEMS[i] = require('database.consumables.potion.' .. item)
    item_menu[i] = ITEMS[i].name
  end
  self.menu = ListMenu(item_menu, {.4,.7,.3})
  self.items = params.items
  self:_show_menu()
end

function ItemSelectState:_show_menu()
  local bfbox = self:view():get('battlefield').bounds
  self.menu:reset_cursor()
  self.menu.position:set(bfbox.right + 32, (bfbox.top + bfbox.bottom) / 2)
  self:view():add('item_menu', self.menu)
end

function ItemSelectState:on_keypressed(key)
  local current = self.menu:current_option()
  self.item = ITEMS[current]
  if key == 'down' then
    SFX.click:stop()
    SFX.click:play()
    self.menu:next()
  elseif key == 'up' then
    SFX.click:stop()
    SFX.click:play()
    self.menu:previous()
  elseif key == 'return' then
    local params = {item = self.item}
    if self.item.buff == 'heal' then
      params.color = 'green'
    elseif self.item.buff == 'mana' then
      params.color = 'blue'
    elseif self.item.buff == 'berserker' then
      params.color = 'red'
    end
    self:view():get('turn_cursor'):pause()
    return self:pop(params)
  elseif key == 'escape' then
    return self:pop({cancel = true})
  end
end

function ItemSelectState:leave()
  self:view():remove('item_menu')
end

return ItemSelectState