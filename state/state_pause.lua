
local State = require 'state'

local StatePause = require 'common.class' (State)

-- Estado de pausa
-- É usado por outros estados para a passagem de um estado
-- para outro não acontecer instantaneamente.
-- os parametros são todos passados de volta para o estado
-- que o chamou
function StatePause:_init(stack)
  self:super(stack)
  self.pause_time = 1
  self.elapsed = 0
  self.params = {}
end

function StatePause:enter(params)
  self.elapsed = 0
  self.params = params
  if params and params.time then
    self.pause_time = params.time
  end
end

function StatePause:update(dt)
  if self.elapsed < self.pause_time then
    self.elapsed = self.elapsed + dt
  else
    -- retorna resume como true, para indicar
    -- ao resume dos outros estado que a pausa
    -- ja foi executada
    if self.params then
      self.params.resume = true
    end
    self:pop(self.params or {resume = true})
  end
end

return StatePause