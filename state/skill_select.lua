
local ListMenu = require 'view.list_menu'
local State = require 'state'

local SKILLS = {} -- skill tables

local SFX = {}

local SkillSelectState = require 'common.class' (State)

function SkillSelectState:_init(stack)
  self:super(stack)
  self.menu = nil
  self.skills = {}
  self.skill = nil
  self.enemies = {}
  self.party = {}
  self.character = nil
  SFX.click = love.audio.newSource('assets/sfx/click4.ogg', 'static')
end

function SkillSelectState:enter(params)
  for i, skill in ipairs(params.skills) do
    SKILLS[i] = require('database.skills.' .. skill)
  end
  self.menu = ListMenu(params.skill_names, {.7,0,.8})
  self.skills = params.skills
  self.enemies = params.enemies
  self.party = params.party
  self.character = params.character
  self:_show_menu()
end

function SkillSelectState:_show_menu()
  local bfbox = self:view():get('battlefield').bounds
  self.menu:reset_cursor()
  self.menu.position:set(bfbox.right + 32, (bfbox.top + bfbox.bottom) / 2)
  self:view():add('skill_menu', self.menu)
end

function SkillSelectState:on_keypressed(key)
  local current = self.menu:current_option()
  self.skill = SKILLS[current]
  if key == 'down' then
    SFX.click:stop()
    SFX.click:play()
    self.menu:next()
  elseif key == 'up' then
    SFX.click:stop()
    SFX.click:play()
    self.menu:previous()
  elseif key == 'return' then
    local mp = self.character:get_mp()
    -- se não tiver mana para usar a skill, apenas muda a mensagem
    -- e não executa a skill. O jogador ainda pode escolher outra skill
    if self.skill.cost > mp then
      self:view():get('message'):set('Not enough mana!')
      return
    end
    local params = {skill = self.skill, targets = self.enemies}
    if self.skill.heal then
      params.color = 'green'
      params.targets = self.party
    end
    self:view():get('turn_cursor'):pause()
    return self:push('target_select', params)
  elseif key == 'escape' then
    return self:pop({cancel = true})
  end
end

function SkillSelectState:resume(params)
  self:view():get('turn_cursor'):unpause()
  if params.cancel then
    return
  end
  params.skill = self.skill
  return self:pop(params)
end

function SkillSelectState:leave()
  self:view():remove('skill_menu')
end

return SkillSelectState