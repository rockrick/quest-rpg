
local PALETTE_DB = require 'database.palette'
local State = require 'state'
local ListMenu = require 'view.list_menu'
local Vec = require 'common.vec'
local Tween = require 'common.tween'

local ChooseQuestState = require 'common.class' (State)

local BGM
local SFX = {}

local LMARGIN = 32

function ChooseQuestState:_init(stack)
  self:super(stack)
  local options = self:_load_quests()
  local h = love.graphics.getHeight()
  self.menu = ListMenu(options)
  self.menu.position = Vec(LMARGIN, h / 2)
  self.vol = 1
end

function ChooseQuestState:_load_quests()
  local options = {}
  local quest_names = love.filesystem.getDirectoryItems('database/quests')
  self.quests = {}
  for i, name in ipairs(quest_names) do
    name = name:sub(1, -5)
    local quest = require('database.quests.' .. name)
    options[i] = quest.title
    self.quests[i] = quest
  end
  return options
end

function ChooseQuestState:enter()
  love.graphics.setBackgroundColor(PALETTE_DB.black)
  BGM = love.audio.newSource('assets/bgm/MainMenu.ogg', 'stream')
  SFX.click = love.audio.newSource('assets/sfx/click4.ogg', 'static')
  BGM:setLooping(true)
  BGM:setVolume(self.vol)
  BGM:play()
  self:view():add('quest_menu', self.menu)
end

function ChooseQuestState:suspend()
  self:view():remove('quest_menu')
end

function ChooseQuestState:resume()
  self.vol = 1
  BGM:setVolume(self.vol)
  BGM:play()
  self:view():add('quest_menu', self.menu)
end

function ChooseQuestState:leave()
  self:view():remove('quest_menu')
end

function ChooseQuestState:on_keypressed(key)
  if key == 'down' then
    SFX.click:stop()
    SFX.click:play()
    self.menu:next()
  elseif key == 'up' then
    SFX.click:stop()
    SFX.click:play()
    self.menu:previous()
  elseif key == 'return' then
    local option = self.menu:current_option()
    self.params = { quest = self.quests[option] }
    self.vol_tween = Tween.new(1, self, {vol = 0}, 'linear')
    -- return self:push('follow_quest', self.params)
  elseif key == 'escape' then
    return self:pop()
  end
end

function ChooseQuestState:update(dt)
  if self.vol_tween then
    BGM:setVolume(self.vol)
    if self.vol_tween:update(dt) then
      self.vol_tween = nil
      BGM:stop()
      return self:push('follow_quest', self.params)
    end
  end
end

return ChooseQuestState


