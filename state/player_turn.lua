
local Vec = require 'common.vec'
local CharacterStats = require 'view.character_stats'
local TurnCursor = require 'view.turn_cursor'
local ListMenu = require 'view.list_menu'
local State = require 'state'

local SFX = {}

local PlayerTurnState = require 'common.class' (State)

local TURN_OPTIONS = { 'Fight', 'Skill', 'Item', 'Run' }

function PlayerTurnState:_init(stack)
  self:super(stack)
  self.character = nil
  self.party = {}
  self.enemies = {}
  self.cursor = nil
  self.menu = ListMenu(TURN_OPTIONS)
  self.atlas = nil
  self.animations = nil
  SFX.click = love.audio.newSource('assets/sfx/click4.ogg', 'static')
end

function PlayerTurnState:enter(params)
  self.character = params.current_character
  self.enemies = params.encounter
  self.party = params.party
  self.atlas = self:view():get('atlas')
  self.animations = self:view():get('animations')
  self:_show_menu()
  self:_show_cursor()
  self:_show_stats()
end

function PlayerTurnState:_show_menu()
  local bfbox = self:view():get('battlefield').bounds
  self.menu:reset_cursor()
  self.menu.position:set(bfbox.right + 32, (bfbox.top + bfbox.bottom) / 2)
  self:view():add('turn_menu', self.menu)
end

function PlayerTurnState:_show_cursor()
  local sprite_instance = self.atlas:get(self.character)
  self.cursor = TurnCursor(sprite_instance)
  self:view():add('turn_cursor', self.cursor)
end

function PlayerTurnState:_show_stats()
  local bfbox = self:view():get('battlefield').bounds
  local position = Vec(bfbox.right + 16, bfbox.top)
  local char_stats = CharacterStats(position, self.character)
  self:view():add('char_stats', char_stats)
end

function PlayerTurnState:leave()
  self:view():remove('turn_menu')
  self:view():remove('turn_cursor')
  self:view():remove('char_stats')
end

function PlayerTurnState:on_keypressed(key)
  local option = TURN_OPTIONS[self.menu:current_option()]
  -- seleciona uma das ações disponiveis no menu
  if key == 'down' then
    SFX.click:stop()
    SFX.click:play()
    self.menu:next()
  elseif key == 'up' then
    SFX.click:stop()
    SFX.click:play()
    self.menu:previous()
  elseif key == 'return' then
    local params = {}
    -- entra no target select e processa um ataque
    if option == 'Fight' then
      params.targets = self.enemies
      self:view():get('turn_cursor'):pause()
      return self:push('target_select', params)
    -- abre o menu de skill para selecionar uma skill
    -- entra no skill select
    elseif option == 'Skill' then
      params = {
        skills = self.character:get_skills(),
        skill_names = self.character:get_skill_names(),
        enemies = self.enemies,
        party = self.party,
        character = self.character
      }
      if #params.skills > 0 then
        self:view():hide('turn_menu')
        return self:push('skill_select', params)
      end
    -- abre o menu de itens para selecionar um item
    -- entra no item select
    elseif option == 'Item' then
      params = {
        items = self.character:get_items(),
        item_names = self.character:get_item_names(),
        character = self.character
      }
      if #params.items >= 0 then
        self:view():hide('turn_menu')
        return self:push('item_select', params)
      end
    -- se escolher run, sai do player turn e foge do encontro atual
    else
      return self:pop({ action = option, character = self.character })
    end
  end
end

-- define os parametros a serem retornados ao estado encounter
-- apos a ação ja ter sido executada
function PlayerTurnState:resume(params)
  self:view():get('turn_cursor'):unpause()
  self:view():unhide('turn_menu')
  if params.cancel then
    return
  end
  local option = TURN_OPTIONS[self.menu:current_option()]
  local ret = {
    action = option,
    character = self.character,
    target = params.target,
    enemies = self.enemies,
  }
  if option == 'Fight' then
    local result = self.character:attack(params.target)
    ret.damage = result.damage
    ret.crit = result.crit
  elseif option == 'Skill' then
    local result = self.character:use_skill(params.skill, params.target)
    ret.message = params.skill.message
    ret.skill = params.skill
    ret.damage = result.damage
    ret.crit = result.crit
    ret.heal = result.heal
    ret.healed_char = result.healed_char
  elseif option == 'Item' then
    self.character:use_item(params.item)
    ret.character = self.character
    ret.message = params.item.message
    ret.item = params.item
  end
  -- roda animações dos personagens
  if ret.damage then
    self.animations:attack(self.character)
    self.animations:take_damage(params.target)
  end
  return self:pop(ret)
end

return PlayerTurnState
