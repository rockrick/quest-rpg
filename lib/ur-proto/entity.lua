
--- An entity managed by the rule engine.
--  @classmod Entity

local ID = require 'ur-proto.id'

local Entity = require 'common.class' ()

function Entity:_init(record, solver)
  rawset(self, 'id', ID())
  rawset(self, 'record', record)
  rawset(self, 'solver', solver)
end

function Entity:__index(key)
  local rulename = key
  if self.record:where('rule', { name = key }).n > 0 then
    return function (_, ...)
      return self.solver:apply(rulename, self.record, self, ...)
    end
  else
    return self.solver:apply('get_' .. rulename, self.record, self)
  end
end

function Entity:__newindex(key, value)
  return self.solver:apply('set_' .. key, self.record, self, value)
end

function Entity:__tostring()
  return ("entity: %d"):format(self.id)
end

return Entity

