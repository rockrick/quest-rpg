
# Manual

If you have any questions, don't hesitate to ask on the
[PACA boards](https://paca.ime.usp.br) or [open an
issue](https://gitlab.com/unlimited-rulebook/ur-proto/issues).

## Installation

Download, clone or submodule [the source
code](https://gitlab.com/unlimited-rulebook/ur-proto) in a `lib/` folder in the
root of your project. It should look more or less like this:

```
project/
  assets/
  common/
  database/
  lib/
    ur-proto/ <-- place it here
  model/
  state/
  view/
  conf.lua
  main.lua
  stack.lua
``` 

After this, add the following lines to your `love.load()`:

```lua
local path = love.filesystem.getRequirePath()
love.filesystem.setRequirePath("lib/?.lua;lib/?/init.lua;" .. path)
```

## Usage

After following the steps above, you can import the unlimited rulebook's
@{RuleEngine} class with:

```lua
local RuleEngine = require 'ur-proto'
```

