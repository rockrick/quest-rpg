
--- Loads a set of rules.
--  @classmod RuleSet

local Entity = require 'ur-proto.entity'
local RuleSet = require 'common.class' ()

local _meta = {}

function RuleSet:_init(setname, record, solver)
  self.setname = setname
  self.record = record
  self.factory = function () return Entity(record, solver) end
  self.define = setmetatable({ set = self }, _meta)
  self.last_rule = nil
end

function RuleSet:new_entity()
  return self.factory()
end

function RuleSet:_define_rule(name, definition)
  local e = self.factory()
  self.record:set(e, 'rule', { name = name,
                               definition = definition })
  self.last_rule = e
end

function RuleSet:get_last_rule()
  return self.last_rule
end

function _meta:__newindex(key, value)
  self.set:_define_rule(key, value)
end

return RuleSet

