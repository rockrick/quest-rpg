
local Character = require 'common.class' ()

function Character:_init(spec)
  self.spec = spec
  self.hp = spec.max_hp -- pontos de vida
  self.mp = spec.max_mp -- pontos de mana
  self.atk = spec.atk -- ataque
  self.spd = spec.spd -- velocidade
  self.def = spec.def -- defesa
  self.ac = spec.ac -- armor class, define a esquiva
  self.skills = spec.skills -- lista de skills
  -- inventario de itens
  self.items = {health_potion = 0, mana_potion = 0, berserker_potion = 0}
  self.list_item = {"health_potion", "mana_potion", "berserker_potion"}
  self.initiative = nil
  self.type = nil
  self.alive = true
end


function Character:get_name()
  return self.spec.name
end

-- tipo do personagem, aliado ou inimigo
function Character:set_type(type)
  self.type = type
end

function Character:get_type()
  return self.type
end

function Character:get_atk()
  return self.spec.atk
end

function Character:get_spd()
  return self.spec.spd
end

function Character:get_def()
  return self.spec.def
end

function Character:get_ac()
  return self.spec.ac
end

function Character:get_skills()
  return self.skills or {}
end

function Character:get_skill_names()
  local names = {}
  for i, skill in pairs(self:get_skills()) do
    local S = require('database.skills.' .. skill)
    names[i] = S.name
  end
  return names
end

function Character:get_items()
  return self.items
end

function Character:get_item_names()
  return self.list_item
end

function Character:generate_initiative()
  self.initiative = math.random(20) + self.spd
end

function Character:get_initiative()
  return self.initiative
end

function Character:get_appearance()
  return self.spec.appearance
end

function Character:get_hp()
  return self.hp, self.spec.max_hp
end

function Character:get_mp()
  return self.mp, self.spec.max_mp
end

function Character:is_alive()
  return self.alive
end

-- cura o personagem em heal pontos
function Character:heal(heal)
  local hp, max_hp = self:get_hp()
  self.hp = math.min(max_hp, hp + heal)
  return heal
end

-- recupera mana pontos de mana do personagem
function Character:mana_regen(mana)
  local mp, max_mp = self:get_mp()
  self.mp = math.min(max_mp, mp + mana)
  return mana
end

-- aumenta o dano do personagem em atk_bonus pontos
function Character:berserker(atk_bonus)
  self.atk = self.atk + atk_bonus
  return atk_bonus
end

function Character.freeze()
end

-- executa um ataque. retorna o resultado do ataque, se foi critico,
-- se damage for nil, o ataque errou
function Character:attack(target, atk)
  if not atk then
    atk = self.atk
  end
  local ret = {damage = nil, crit = false}
  local hit = math.random(20)
  ret.crit = hit == 20
  hit = hit + self.spd
  if hit > target:get_ac() or ret.crit then
    -- acertou
    ret.damage = target:take_damage(atk, target:get_def(), ret.crit)
  end
  return ret
end

-- leva o dano, se o hp chegar em 0, o personagem morre
function Character:take_damage(atk, target_def, crit)
  local damage = math.max(1, math.floor(atk*(20/(20+target_def))))
  if crit then
    damage = damage*2
  end
  self.hp = math.max(self.hp - damage, 0)
  if self.hp == 0 and self.alive then
    self:die()
  end
  return damage
end

-- processa uma skill no alvo target
function Character:use_skill(skill, target)
  local result = {}
  if skill.atk then
    result = self:attack(target, skill.atk)
  elseif skill.heal then
    result.heal = target:heal(skill.heal)
    result.healed_char = target
  end
  if skill.eff == 'drain' then
    result = self:attack(target, skill.atk)
    result.heal = self:heal(skill.eff_info)
    result.healed_char = self
  elseif skill.eff == 'freeze' then
    target:freeze(skill.eff_info)
  end
  self.mp = self.mp - skill.cost
  return result
end

-- usa um item
function Character:use_item(item)
  local result = {}
  if item.buff == 'heal' then
    result.heal = self:heal(item.quantity)
  elseif item.buff == 'mana' then
    result.heal = self:mana_regen(item.quantity)
  elseif item.buff == 'berserker' then
    result.heal = self:berserker(item.quantity)
  end
  return result
end

-- mata o personagem de uma forma brutal e horrivel
function Character:die()
  self.alive = false
  print(self:get_name() .. " died!")
end

return Character