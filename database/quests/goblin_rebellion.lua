
return {
  title = 'Goblin Rebellion',
  party = { 'warrior', 'archer', 'priest' },
  encounters = {
    { 'goblin', 'young_goblin' },
    { 'goblin', 'goblin', 'goblin' },
    { 'angry_goblin', },
    { 'young_goblin', 'hobgoblin' },
    { 'goblin', 'hobgoblin' },
  }
}

