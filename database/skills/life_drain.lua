
return {
  name = "Life Drain",
  cost = 6,
  atk = 4,
  eff = 'drain', -- effect
  eff_info = 4, -- heal 4 hp
  message = "%s drained life from %s!"
}