
return {
  name = "Goblin",
  appearance = 'goblin',
  max_hp = 9,
  max_mp = 5,
  atk = 5,
  spd = 7,
  def = 4,
  ac = 9, -- armor class, define a esquiva
}

