
return {
  name = "Young Goblin",
  appearance = 'young_goblin',
  max_hp = 6,
  max_mp = 5,
  atk = 3,
  spd = 6,
  def = 3,
  ac = 7, -- armor class, define a esquiva
}

