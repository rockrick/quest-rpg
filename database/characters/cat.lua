
return {
  name = "Cat",
  appearance = 'cat',
  max_hp = 80,
  max_mp = 80,
  atk = 1,
  spd = 100,
  def = 100,
  ac = 30, -- armor class, define a esquiva
}

