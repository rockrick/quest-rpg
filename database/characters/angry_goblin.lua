
return {
  name = "Angry Goblin",
  appearance = 'angry_goblin',
  max_hp = 13,
  max_mp = 5,
  atk = 7,
  spd = 12,
  def = 4,
  ac = 8, -- armor class, define a esquiva
}

