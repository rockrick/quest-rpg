
return {
  name = "Chicken",
  appearance = 'chicken',
  max_hp = 2,
  max_mp = 0,
  atk = 2,
  spd = 8,
  def = 0,
  ac = 2, -- armor class, define a esquiva
}

