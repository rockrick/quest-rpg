
return {
  name = "Hobgoblin",
  appearance = 'hobgoblin',
  max_hp = 16,
  max_mp = 5,
  atk = 5,
  spd = 9,
  def = 5,
  ac = 12, -- armor class, define a esquiva
}

