
return {
  texture = 'kenney-1bit.png',
  frame_width = 16,
  frame_height = 16,
  gap_width = 1,
  gap_height = 1,
  sprites = {
    invalid = {
      frame = { 24, 25 },
      color = 'red'
    },

    cat = {
      frame = { 30, 7 },
      color = 'white'
    },
    chicken = {
      frame = { 26, 7 },
      color = 'real_white'
    },

    slime = {
      frame = { 27, 8 },
      color = 'green'
    },
    blue_slime = {
      frame = { 27, 8 },
      color = 'blue'
    },

    goblin = {
      frame = { 25, 2 },
      color = 'green'
    },
    young_goblin = {
      frame = { 29, 2 },
      color = 'green'
    },
    angry_goblin = {
      frame = { 26, 2 },
      color = 'red'
    },
    hobgoblin = {
      frame = { 30, 2 },
      color = 'green'
    },

    knight = {
      frame = { 28, 0 },
      color = 'blue'
    },
    archer = {
      frame = { 32, 0 },
      color = 'red'
    },
    priest = {
      frame = { 24, 0 },
      color = 'white'
    },
    business_guy = {
      frame = { 28, 4 },
      color = 'grey'
    },
  }
}

