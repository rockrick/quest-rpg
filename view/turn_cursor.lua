
local Vec = require 'common.vec'
local Tween = require 'common.tween'
local TurnCursor = require 'common.class' ()
local Palette = require 'database.palette'

function TurnCursor:_init(selected_drawable, color)
  self.offset = Vec(0, -48)
  self.tween = 0
  self.selected_drawable = selected_drawable
  if type(color) == 'string' then
    self.color = Palette[color]
  elseif type(color) == 'table' then
    self.color = color
  else
    self.color = {1,1,1}
  end
  self.paused = false
  self.t1 = Tween.new(0.3, self, {offset = Vec(0,-58)}, 'outQuad')
  self.f1 = false -- 1 has finished?
  self.t2 = Tween.new(0.3, self, {offset = Vec(0,-48)}, 'inQuad')
  self.f2 = false
end

function TurnCursor:draw()
  if self.selected_drawable then
    local g = love.graphics
    local position = self.selected_drawable.position + self.offset
    g.push()
    g.translate(position:get())
    g.setColor(self.color)
    g.polygon('fill', -8, 0, 8, 0, 0, 8)
    g.pop()
  end
end

function TurnCursor:pause()
  self.paused = true
  self.color = {1,1,1,0.2}
end

function TurnCursor:unpause()
  self.paused = false
  self.color = {1,1,1,1}
end

function TurnCursor:update(dt)
  if not self.paused then
    if not self.f1 then
      self.f1 = self.t1:update(dt)
    elseif not self.f2 then
      self.f2 = self.t2:update(dt)
    else
      self.t2:reset()
      self.t1:reset()
      self.f1 = false
      self.f2 = false
    end
  end
end

return TurnCursor

