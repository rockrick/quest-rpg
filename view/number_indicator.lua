
local Vec = require 'common.vec'
local Tween = require 'common.tween'
local NumberIndicator = require 'common.class' ()

-- mostra numeros flutuantes para indicar numeros como dano,
-- cura, mana e também efeitos de status, como berserker
function NumberIndicator:_init()
  self.tweens = {}
  self.font = love.graphics.newFont('assets/fonts/VT323-Regular.ttf', 36)
  self.font:setFilter('nearest', 'nearest')
end

-- cada uma dessas funções esta encarregada de um atributo diferente,
-- podendo assim termos mais de um indicador ao memso tempo, como
-- no caso da skill life drain
function NumberIndicator:show_berserker(target_sprite)
  local display = "BERSERKER!"
  self.berserker_offset = Vec(-58, -48)
  self.berserker_alpha = 1
  self.tweens.berserker_offset = Tween.new(0.8, self,
  {berserker_offset = Vec(-58, -58)}, 'outQuad')
  self.tweens.berserker_alpha = Tween.new(0.8, self,
  {berserker_alpha = 0}, 'inQuad')
  self.berserker_display = love.graphics.newText(self.font, display)
  self.berserker_pos = Vec(target_sprite.position:get())
end

function NumberIndicator:show_mana(target_sprite, value)
  local display = tostring(value)
  self.mana_offset = Vec(-24, -48)
  self.mana_alpha = 1
  self.tweens.mana_offset = Tween.new(0.5, self,
  {mana_offset = Vec(-24, -58)}, 'outQuad')
  self.tweens.mana_alpha = Tween.new(0.5, self,
  {mana_alpha = 0}, 'inQuad')
  self.mana_display = love.graphics.newText(self.font, display)
  self.mana_pos = Vec(target_sprite.position:get())
end

function NumberIndicator:show_heal(target_sprite, value)
  local display = tostring(value)
  self.heal_offset = Vec(-24, -48)
  self.heal_alpha = 1
  self.tweens.heal_offset = Tween.new(0.5, self,
  {heal_offset = Vec(-24, -58)}, 'outQuad')
  self.tweens.heal_alpha = Tween.new(0.5, self,
  {heal_alpha = 0}, 'inQuad')
  self.heal_display = love.graphics.newText(self.font, display)
  self.heal_pos = Vec(target_sprite.position:get())
end

function NumberIndicator:show_damage(target_sprite, value, crit)
  local display
  self.dam_offset = Vec(-24, -48)
  self.dam_alpha = 1
  if value then
    display = tostring(value)
    if crit then
      display = display .. '!'
    end
  else
    display = 'miss'
  end
  self.tweens.dam_offset = Tween.new(0.5, self,
  {dam_offset = Vec(-24, -58)}, 'outQuad')
  self.tweens.dam_alpha = Tween.new(0.5, self,
  {dam_alpha = 0}, 'inQuad')
  self.dam_display = love.graphics.newText(self.font, display)
  self.dam_pos = Vec(target_sprite.position:get())
end

-- desenha todos os indicadores ativos
function NumberIndicator:draw()
  local g = love.graphics
  local position
  if self.dam_display then
    position = self.dam_pos + self.dam_offset
    g.push()
    g.setColor( 1, .4, .4, self.dam_alpha)
    g.translate(position:get())
    g.draw(self.dam_display)
    g.pop()
  end
  if self.heal_display then
    position = self.heal_pos + self.heal_offset
    g.push()
    g.setColor( .4, 1, .4, self.heal_alpha)
    g.translate(position:get())
    g.draw(self.heal_display)
    g.pop()
  end
  if self.mana_display then
    position = self.mana_pos + self.mana_offset
    g.push()
    g.setColor( .4, .4, 1, self.mana_alpha)
    g.translate(position:get())
    g.draw(self.mana_display)
    g.pop()
  end
  if self.berserker_display then
    position = self.berserker_pos + self.berserker_offset
    g.push()
    g.setColor( 1, .2, .2, self.berserker_alpha)
    g.translate(position:get())
    g.draw(self.berserker_display)
    g.pop()
  end
end

function NumberIndicator:update(dt)
  for _, tween in pairs(self.tweens) do
    tween:update(dt)
  end
end

return NumberIndicator

